#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "fonctions_game.h"


#define FHAUT 80
#define FBAS 72
#define FGAUCHE 77
#define FDROITE 75

int tableau[TAILLE*TAILLE] = {0};

int fDirection(direction d, int x, int y) {
    switch (d) {
        case Droite:
            return y*TAILLE+x;
        case Gauche:
            return y*TAILLE+(TAILLE-x-1);
        case Bas:
            return x*TAILLE+y;
        case Haut:
            return (TAILLE-x-1)*TAILLE+y;
        default:
            break;
    }
    return 0;
}

direction memChoix() {
    do {
        //system("cls");

        switch(getch()) {
            case FHAUT:
                return Haut;
                break;
            case FBAS:
                return Bas;
                break;
            case FGAUCHE:
                return Gauche;
                break;
            case FDROITE:
                return Droite;
                break;
        }
    } while(true);
}

void affichageJeu() {
    int i, j;
    for(int a = 0; a < TAILLE; a++) {
        printf("+-----");
    }
    printf("+\n");

    for(j = 0; j < TAILLE; j++) {
        for(i = 0; i < TAILLE; i++) {
            int val = tableau[fDirection(Droite, i, j)];
            if(i == 0)
                printf("|");
            if (val == 0) {
                printf("     |");
            }
            else
            {
                printf("%4d |", val);
            }
        }
        printf("\n");
        for(int a = 0; a < TAILLE; a++) {
            printf("+-----");
        }
        printf("+\n");
    }
    printf("\n\n\n");
}

void deuxOuQuatre()
{
    int aleatoire, valeur;
    do
    {
        aleatoire = rand()%(TAILLE*TAILLE);
    }while (tableau[aleatoire]!=0);

    valeur = rand()%(2-0);
    if(valeur == 1 ) tableau[aleatoire] = 2;
    else tableau[aleatoire] = 4;
}

int fusionDirection(direction choix, int y, int fusion, int verification)
{
    for(int i = 1; i < TAILLE; i++)
    {
        int case_a = tableau[fDirection(choix, i, y)];
        int case_b = tableau[fDirection(choix, i-1, y)];
        if ((fusion == 0 && case_b == 0 && case_a > 0) || (fusion == 1 && case_b == case_a && case_a != 0))
        {
            if(verification)
                return 1;
            tableau[fDirection(choix, i-1, y)] = case_a + case_b;
            tableau[fDirection(choix, i, y)] = 0;
            if(fusion == 0)
                i = 0;  // recommence.
        }
    }
    return 0;
}

int deplacement(direction choix)
{
    for(int i = 0; i < TAILLE; i++)
    {
        fusionDirection(choix, i, 0, 0);
        fusionDirection(choix, i, 1, 0);
        fusionDirection(choix, i, 0, 0);
    }
    return 0;
}

int possible(direction choix)
{
    int i;
    for(i = 0; i < TAILLE ;i++)
        if (fusionDirection(choix, i, 0, 1) || fusionDirection(choix, i, 1, 1))
            return 1;
    return 0;
}

bool jeuPerdu()
{
    return !(possible(Droite) || possible(Gauche) || possible(Bas) || possible(Haut));
}

bool jeuGagne()
{
    for(int j = 0; j < TAILLE; j++)
        for(int i = 0; i < TAILLE; i++)
            if(tableau[fDirection(Droite, i, j)] == 2048)
                return true;
    return false;
}
