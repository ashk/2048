#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TAILLE 5
#define FHAUT 80
#define FBAS 72
#define FGAUCHE 77
#define FDROITE 75

enum direction
{
    Droite,
    Gauche,
    Bas,
    Haut
};

int fDirection(direction d, int x, int y);

void affichageJeu();

void deuxOuQuatre();

int fusionDirection(direction choix, int y, int fusion, int verif);

int deplacement(direction choix);

int possible(direction choix);

bool jeuPerdu();

bool jeuGagne();

direction memChoix();

