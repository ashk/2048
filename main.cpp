#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "fonctions_game.h"


#define FHAUT 80
#define FBAS 72
#define FGAUCHE 77
#define FDROITE 75

int main()
{
    deuxOuQuatre();
    deuxOuQuatre();
    while(!jeuPerdu() && !jeuGagne())
    {
        direction choix;
        affichageJeu();
        do
        {
            choix = memChoix();
        } while (!possible(choix));
        deplacement(choix);
        deuxOuQuatre();
    }

    if(jeuPerdu())
        printf("Partie perdu");
    else(jeuGagne())
        printf("Partie gagnee");

    return 0;
}


